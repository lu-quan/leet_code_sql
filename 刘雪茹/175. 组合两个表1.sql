﻿/*175. 组合两个表
表1: Person
PersonId 是上表主键
+-------------+---------+
| 列名         | 类型     |
+-------------+---------+
| PersonId    | int     |
| FirstName   | varchar |
| LastName    | varchar |
+-------------+---------+
表2: Address
AddressId 是上表主键 
+-------------+---------+
| 列名         | 类型    |
+-------------+---------+
| AddressId   | int     |
| PersonId    | int     |
| City        | varchar |
| State       | varchar |
+-------------+---------+
*/
Create table Person
(
  PersonId  int Primary key identity(1,1),
  FirstName varchar(10),
  LastName  varchar(10)
)

Create table Address
(
  AddressId  int Primary key identity(1,1),
  PersonId   int references Person(PersonId) not null  , 
  City varchar(100) not null,
  State  varchar(100) not null
)  
insert into Person( FirstName, LastName)
 Select'小芳','芳芳' Union
 Select'小Q','QQ' Union
 Select'小X','大X' 

if exists(select*from sys.objects where name='Address')
 Drop table Address
Create table Address
(
  AddressId  int Primary key identity(1,1),
  PersonId   int references Person(PersonId) not null  , 
  City varchar(100) not null,
  State  varchar(100) not null,
  Information varchar(200) null
)  
  

insert into Address( PersonId,City,State,Information)
 Select'001','北京','中国','亚洲' Union
 Select'002','华盛顿','美国','' Union
 Select'003','伦敦','英国','' 

/*编写一个 SQL 查询，满足条件：无论 person 是否有地址信息，
都需要基于上述两表提供 person 的以下信息：FirstName, LastName, City, State*/

Select  Person.FirstName, Person.LastName, Address.City, Address.State, Address.Information from Person
left join Address on Person.PersonId = Address.PersonId 