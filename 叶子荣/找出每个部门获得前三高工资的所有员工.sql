					
					
					
					
		--找出每个部门获得前三高工资的所有员工
		select "D".deptno,"E".ename,"E".sal
			from 
			(
			select ename,sal,deptno,dense_rank() over(partition by deptno order by sal desc)
				as Ranking from emp 
			)as "E"
			join dept"D"
			on "D".deptno = "E".deptno
			where "E".Ranking<=3