use Leetcode

create table Person (
	ID int not null primary key identity ,
	Email varchar(80) not null ,
)

insert into Person (Email) values ('john@example.com') ,('bob@example.com'),('john@example.com')

select * from Person 

select a.Email  from Person a, Person b 
where a.Email=b.Email and a.ID>b.ID

delete a from Person a ,Person b 
where a.Email=b.Email and a.ID>b.ID