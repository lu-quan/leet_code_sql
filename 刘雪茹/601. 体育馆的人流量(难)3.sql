/*601. 体育馆的人流量
X 市建了一个新的体育馆，每日人流量信息被记录在这三列信息中：序号 (id)、日期 (visit_date)、 人流量 (people)。
请编写一个查询语句，找出人流量的高峰期。高峰期时，至少连续三行记录中的人流量不少于100。
例如，表 stadium：
+------+------------+-----------+
| id   | visit_date | people    |
+------+------------+-----------+
| 1    | 2017-01-01 | 10        |
| 2    | 2017-01-02 | 109       |
| 3    | 2017-01-03 | 150       |
| 4    | 2017-01-04 | 99        |
| 5    | 2017-01-05 | 145       |
| 6    | 2017-01-06 | 1455      |
| 7    | 2017-01-07 | 199       |
| 8    | 2017-01-08 | 188       |
+------+------------+-----------+
对于上面的示例数据，输出为：
+------+------------+-----------+
| id   | visit_date | people    |
+------+------------+-----------+
| 5    | 2017-01-05 | 145       |
| 6    | 2017-01-06 | 1455      |
| 7    | 2017-01-07 | 199       |
| 8    | 2017-01-08 | 188       |
+------+------------+-----------+
提示：
每天只有一行记录，日期随着 id 的增加而增加。
*/
create table stadium
(
  id int identity(1,1),
  visit_date smalldatetime not null,	
  people int
)
alter table stadium alter column visit_date date

insert into stadium( visit_date, people)
   select '2017-01-01','10' union
   select '2017-01-02','109' union
   select '2017-01-03','150' union
   select '2017-01-04','99' union
   select '2017-01-05','145' union
   select '2017-01-06','1455' union
   select '2017-01-07','199' union
   select '2017-01-08','188' 

select*from stadium 

select id, visit_date ,people 
from ( select id,visit_date,people,count(id) over(partition by rn_offset) cnt
    from (select*,row_number() over(order by id) - id rn_offset
        from stadium
        where people >= 100
    ) t
) t1
where cnt >= 3
order by id




