create table Empployeee
(
	Id int primary key not null,
	Name varchar(20) not null,
	Salary int not null,
	DepartmentId int not null,
)

create table Departmentp
(
	Id int not null references Employeee(Id),
	Name varchar(20) not null,
)

insert Empployeee values('1','Joe','70000','1')
insert Empployeee values('2','Henry','80000','2')
insert Empployeee values('3','Sam','60000','2')
insert Empployeee values('4','Max','90000','1')

insert Departmentp values('1','IT')
insert Departmentp values('2','Sales')

select DepartmentId, MAX(Salary) as Salary from Empployeee group by DepartmentId;

select Department,Empployeee,Salary from (SELECT
d.Name Department,
e.Name Empployeee,
Salary,
DENSE_RANK () OVER (
partition BY DepartmentId
ORDER BY
Salary DESC
) AS rn
FROM
Empployeee e INNER JOIN Department d on DepartmentId=d.Id) as em where   rn=1
