 /* create table Weather (
Id int identity(1,1) primary key not null ,
RecordDate nvarchar(30) not null,
Temperature int not null ,
)

insert into Weather(RecordDate,Temperature) values ('2015-01-01',10)
insert into Weather(RecordDate,Temperature) values ('2015-01-02',25)
insert into Weather(RecordDate,Temperature) values ('2015-01-03',20)
insert into Weather(RecordDate,Temperature) values ('2015-01-04',30)
*/

--注意 Datediff  SQL Serve的datediff(时间单位, 减数, 被减数)必须要三个参数。
select b.Id
from Weather as a,Weather as b
where a.Temperature<b.Temperature and datediff(day,a.RecordDate,b.RecordDate)=1
