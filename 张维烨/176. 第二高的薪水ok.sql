﻿/*
编写一个 SQL 查询，获取 Employee 表中第二高的薪水（Salary） 。

+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
例如上述 Employee 表，SQL查询应该返回 200 作为第二高的薪水。如果不存在第二高的薪水，那么查询应返回 null。

+---------------------+
| SecondHighestSalary |
+---------------------+
| 200                 |
+---------------------+
*/


create table Employee
(
	Id int primary key identity,
	Salary int
)


insert into Employee(Salary)
select 100 union
select 200 union
select 300 ;


--解决方案
select max(Salary) as SecondHighestSalary 
from Employee
where Salary < (
    select max(Salary) from Employee
);--只适用本题情况 遇到大量的数据就很鸡肋



