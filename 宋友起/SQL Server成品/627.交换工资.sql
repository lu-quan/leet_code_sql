create table salary(id int, name varchar(100), sex char(1), salary int)

insert into salary (id, name, sex, salary) values ('1', 'A', 'm', '2500')
insert into salary (id, name, sex, salary) values ('2', 'B', 'f', '1500')
insert into salary (id, name, sex, salary) values ('3', 'C', 'm', '5500')
insert into salary (id, name, sex, salary) values ('4', 'D', 'f', '500')
select *
from salary
update salary set sex='f' where sex ='m';
update salary set sex='m' where sex ='f';

update salary 
set sex = (
            case when
             sex = 'm' then 'f';      --如果是m 改成f
             else 'm'			      -- 否则改成m

            end
          )  
