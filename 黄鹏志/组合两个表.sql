use Ppz
go

create table Person_one
(
	PersonId int primary key,
	FirstName varchar(20) not null,		
	LastName varchar(20) not null
)
create table Address_ones
(
	AddressId int primary key not null,
	PersonId int not null references Person_one(PersonId),
	City varchar(20) not null,
	State varchar(20) not null
)
insert Person_one values('1001','威廉','史密斯')
insert Person_one values('1002','小波','屁屁志')
insert Person_one values('1003','威化','史大斯')
insert Person_one values('1004','佐罗','有点黑')

insert Address_ones values('1','1001','英国','阿拉斯加')
insert Address_ones values('2','1002','中国','爱情')
insert Address_ones values('3','1003','法国','法法家加')
insert Address_ones values('4','1004','美国国','施工斯加')

select * from Person_one,Address_ones
select * from Person_one,Address_ones where Person_one.PersonId =Address_ones.PersonId