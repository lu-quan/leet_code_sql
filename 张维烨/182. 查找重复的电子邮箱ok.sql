﻿/*
编写一个 SQL 查询，查找 Person 表中所有重复的电子邮箱。

示例：

+----+---------+
| Id | Email   |
+----+---------+
| 1  | a@b.com |
| 2  | c@d.com |
| 3  | a@b.com |
+----+---------+
根据以上输入，你的查询应返回以下结果：

+---------+
| Email   |
+---------+
| a@b.com |
+---------+
说明：所有电子邮箱都是小写字母。
*/


create table Person182
(
	Id int primary key identity,
	Email varchar(255)
)

insert into Person182(Email) values('a@b.com');
insert into Person182(Email) values('c@d.com');
insert into Person182(Email) values('a@b.com');


--解决方案
select Email from Person182 where Email='a@b.com' 
order by Id --此处必不可少！！！
offset 0 rows fetch next 1 rows only;

select distinct p1.Email from Person182 as p1,Person182 as p2 where p1.Email =p2.Email and p1.Id!=p2.Id;
--整个查询代码OK 可以完成