select top 1 sal
	from emp
		where sal not in 
		(
		select max(sal)
			from emp
		)
		order by sal desc