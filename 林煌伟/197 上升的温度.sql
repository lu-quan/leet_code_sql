use Leetcode 

create table Weather (
	ID int not null primary key identity ,
	RecordDate date not null ,
	Temperature int not null,
)

insert into Weather (RecordDate,Temperature) values ('2015-01-01',10),('2015-01-02',25),('2015-01-03',20),('2015-01-04',30)

select * from Weather 

select a.ID from Weather a join Weather b 
on datediff (day, a.RecordDate, b.RecordDate) =-1
and a.Temperature>b.Temperature

select b.ID  from Weather a join Weather b 
on datediff (day, a.RecordDate, b.RecordDate) =1
and a.Temperature<b.Temperature











