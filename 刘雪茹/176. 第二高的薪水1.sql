﻿/*176. 第二高的薪水
编写一个 SQL 查询，获取 Employee 表中第二高的薪水（Salary） 。
--+----+--------+
--| Id | Salary |
--+----+--------+
--| 1  | 100    |
--| 2  | 200    |
--| 3  | 300    |
--+----+--------+

*/
if exists(select*from sys.objects where name='Employee')
 Drop table Employee
Create table Employee
(
  Id int primary key identity(1,1),
  Salary decimal(10,2) not null
)
if exists(select*from sys.objects where name='Employee')
 Drop table Employee
Create table Employee
(
  Id int primary key ,
  Salary decimal(10,2) not null
)
insert  Employee values ('1','100')
insert  Employee values ('2','200')
insert  Employee values ('3','300')
select*from Employee
--例如上述 Employee 表，SQL查询应该返回 200 作为第二高的薪水 ,如果不存在第二高的薪水，那么查询应返回 nul。               
--+---------------------+
--| SecondHighestSalary |
--+---------------------+
--| 200                 |
--+---------------------+

select max(Salary) as SecondHighestSalary from Employee 
where Salary<(select max(Salary) from Employee)

select nullif((select salary from (select salary,row_number () over (order by salary desc) as h 
from (select distinct salary from employee) g) a where h = 2),null) as SecondHighestSalary


