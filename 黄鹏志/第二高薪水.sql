use Ppz
go

create table Employee_one
(
	Id int primary key not null,
	Salary int not null,
)
insert Employee_one values('1001',100)
insert Employee_one values('1002',200)
insert Employee_one values('1003',300)
insert Employee_one values('1004',400)

select max(Salary) as SecondHighestSalary from Employee_one 
where Salary<(select max(Salary) from Employee_one)