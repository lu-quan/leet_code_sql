create table Logs
(
	Id int not null,
	Num int not null,
)

insert Logs values('1','1')
insert Logs values('2','1')
insert Logs values('3','1')
insert Logs values('4','2')
insert Logs values('5','4')
insert Logs values('6','3')
insert Logs values('7','5')
insert Logs values('8','6')

select * from Logs

select Num
from (
	select Num, 
		row_number() over(order by Id) 
		- row_number() over(partition by Num order by Id) as Id
	from Logs) F
group by Num
having count(*)>=3
