/*184. 部门工资最高的员工
Employee3 表包含所有员工信息，每个员工有其对应的 Id, salary 和 department Id。
+----+-------+--------+--------------+
| Id | Nam  | Salary | DepartmentId |
+----+-------+--------+--------------+
| 1  | Joe   | 70000  | 1            |
| 2  | Henry | 80000  | 2            |
| 3  | Sam   | 60000  | 2            |
| 4  | Max   | 90000  | 1            |
+----+-------+--------+--------------+
Department2 表包含公司所有部门的信息。
+----+----------+
| Id | Name     |
+----+----------+
| 1  | IT       |
| 2  | Sales    |
+----+----------+
编写一个 SQL 查询，找出每个部门工资最高的员工。
例如，根据上述给定的表格，Max 在 IT 部门有最高工资，Henry 在 Sales 部门有最高工资。
+------------+----------+--------+
| Department | Employee3 | Salary |
+------------+----------+--------+
| IT         | Max      | 90000  |
| Sales      | Henry    | 80000  |
+------------+----------+--------+
*/
create table Employee3 
(
  Id int primary key identity(1,1),
  Name nvarchar(20) not null,
  Salary decimal(7,0) not null,
  DepartmentId int
)
   alter table  Employee3 drop column  Name
   alter table  Employee3 add Nam varchar(200) 


create table Department2 
(
  Id int primary key not null,
  Name nvarchar(20) not null
)

/*select*from Employee3
SELECT d.Name AS Department2, a.Id AS Employee3, e.Salary AS Salary
FROM(SELECT MAX(Salary) AS Salary, Id FROM Employee3 GROUP BY Id) AS a 
JOIN Employee3 AS e ON a.Id = e.Id AND a.Salary = e.Salary
JOIN Department2 AS d ON e.Id = d.Id

select d.Name AS Department2, e.Nam AS Employee3, e.Salary AS Salary
from
(select d.Name* d.Id department2,
RANK() over(partition by e.departmentid order by salary desc) RN
from Employee3 e inner join Department2 d
on e.departmentid=d.id) temp
where temp.RN=1;
