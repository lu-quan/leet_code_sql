create table Person (
	ID int primary key not null identity ,
	Email varchar (80) not null ,
)

insert into Person (Email) values ('a@b.com'),('c@d,com'),('a@b.com')

select Email from Person 
group by Email 
having count(Email)>1
